#include<stdio.h>
#define ChangingCost 3
#define FixedCost 500

int Profit(int price);

int Viewers(int price);

int Income(int price);

int Costamt(int price);



int	Viewers(int price)
{
    return 120-((price-15)/5)*20;
}


int Income(int price)
{
    return Viewers(price)*price;
}


int Costamt(int price)
{
    return Viewers(price)*ChangingCost+FixedCost;
}


int Profit(int price)
{
    return Income(price)-Costamt(price);
}


int main()
{
    int price;
    printf("Profit derived according to ticket price:\n\n");
    for(price=5; price<=45; price+=5)
    {
        printf("Ticket = Rs.%d \t\t\t Profit = Rs.%d\n",price,Profit(price));
        printf("\n");
    }





    return 0;
}




